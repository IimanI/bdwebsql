#Gestion des données pour le WEB Mysql/Doctrine

##Installation

- Copier le dossier bdwebsql à la racine de votre serveur WEB.
- Créer la base de données le dump se trouve dans le dossier bd.
- Installer Composer.
- Ouvrir un terminal dans le dossier copié précédemment.
- Effectuer un `php composer.phar install`
- Renseigner les informations de connexion à votre base de données dans index.php .


##Informations

Application utilisant Silex, Doctrine et Angular JS pour afficher des produits après avoir sélectionnés la famille de ceux-ci dans un menu. Le but est de comparer les performances de 2 applications équivalentes dans leur structure mais utilisant pour l'une Mysql à travers un ORM et pour l'autre MongoDB.