<?php
/**
 * Created by PhpStorm.
 * User: iman
 * Date: 25/04/15
 * Time: 22:03
 */


namespace bdwebsql\model;

/**
 * Class Tva
 * @package model
 * @Entity @Table(name="tva")
 */
class Tva {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="float")
     */
    protected $tx_tva;


    public function getId(){

        return $this->id;
    }


    public function getTxTva(){

        return $this->tx_tva;
    }

    public function setTxTva($tx_tva){

        $this->tx_tva = $tx_tva;
    }

}