controllerApp = angular.module('IndexCtrl', [])

controllerApp.controller 'PanelCtrl', ['$scope', 'familiesSrv', 'articlesSrv', ($scope, familiesSrv, articlesSrv) ->
  @selectFamilyN1 = (depth, idParent) ->
    $scope.acti = true
    $scope.acti2 = false
    $scope.acti3 = false
    $scope.acti4 = false
    $scope.idParentN2 = idParent
    $scope.familiesN2 = {}
    familiesSrv.query({depth: depth}).$promise.then (response) ->
      $scope.familiesN2 = response
  @selectFamilyN2 = (depth, idParent) ->
    $scope.acti = true
    $scope.acti2 = true
    $scope.acti3 = false
    $scope.acti4 = false
    $scope.idParentN3 = idParent
    $scope.familiesN3 = {}
    familiesSrv.query({depth: depth}).$promise.then (response) ->
      $scope.familiesN3 = response
  @selectFamilyN3 = (depth, idParent) ->
    $scope.acti = true
    $scope.acti2 = true
    $scope.acti3 = true
    $scope.acti4 = false
    $scope.idParentN4 = idParent
    $scope.familiesN4 = {}
    familiesSrv.query({depth: depth}).$promise.then (response) ->
      $scope.familiesN4 = response
  @selectFamilyN4 = (depth, idParent) ->
    $scope.acti = true
    $scope.acti2 = true
    $scope.acti3 = true
    $scope.acti4 = true
    $scope.idParentN5 = idParent
    $scope.familiesN5 = {}
    familiesSrv.query({depth: depth}).$promise.then (response) ->
      $scope.familiesN5 = response
  @getArticles = (idFamily) ->
    articlesSrv.query({id_family: idFamily}).$promise.then (data) ->
      $scope.articles = data
]
