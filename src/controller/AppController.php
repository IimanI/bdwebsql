<?php

namespace bdwebsql\controller;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class AppController {

    public function viewHome(Request $request, Application $app){
        echo '<!DOCTYPE html>
                <html lang="fr" ng-app="dataWebApp">
                    <head>
                        <meta charset="UTF-8">
                        <title>Données pour le WEB SQL</title>
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
                        <link rel="stylesheet" href="webroot/css/style.css">
                    </head>
                    <body>
                        <header>
                            <h1>Gestion de données pour le WEB<small> Mysql/Doctrine 2</small></h1>
                        </header>
                        <section ng-view></section>
                        <footer>
                            Licence CISIIE - Oo2015oO - JEROME Cyril
                        </footer>
                    </body>
                    <script src="webroot/lib/angular.js"></script>
                    <script src="https://code.angularjs.org/1.3.15/angular-route.js"></script>
                    <script src="https://code.angularjs.org/1.3.15/angular-resource.js"></script>
                    <script src="webroot/angular/dataWebApp.js"></script>
                    <script src="webroot/angular/controller/indexCtrl.js"></script>
                    <script src="webroot/angular/service/indexService.js"></script>
                </html>';
        return false;
    }

    /**
     * recupére tous les articles d'une famille
     * @param $id_family
     * @return object JSON
     */
    public function getArticlesByFamily($id_family, $app) {

//        récup d'une entityManager
        $config = Setup::createAnnotationMetadataConfiguration($app['config']['path'], $app['config']['isDevMode']);
        $entityManager = EntityManager::create($app['db.option'], $config);

//        requête pour obtenir tous les articles d'une famille
        $family = $entityManager->find('bdwebsql\model\Family', $id_family);
        $articles = $family->getArticles();

//        mise en forme des données
        foreach ($articles as $article) {
            $reference = $article->getReference();
            $label = $article->getLabel();
            $pu_ht = $article->getPuHt();
            $tva = $article->getTva()->getTxTva();
            $marque = $article->getMarque()->getLibelle();
            $tabArt = array('reference' => $reference, 'label' => $label, 'prix' => $pu_ht, 'tva' => $tva, 'marque' => $marque);
            $tabArticles[] = $tabArt;
        }
        if (isset($tabArticles)){
            return $app->json($tabArticles);
        }else{
            return false;
        }
    }

    /**
     * recupére une famille en fonction de son parent
     * @param $depth
     * @param $app
     * @return object JSON
     * @throws \Doctrine\ORM\ORMException
     */
    public function FamilyByDepth($depth, $app) {
        //        récup d'une entityManager
        $config = Setup::createAnnotationMetadataConfiguration($app['config']['path'], $app['config']['isDevMode']);
        $entityManager = EntityManager::create($app['db.option'], $config);

//        requête pour obtenir toutes les familles en fonction de leur profondeur et de l'id de leur parent

        $families = $entityManager
            ->getRepository('bdwebsql\model\Family')
            ->findBy(array('depth' => $depth));
//        mise en forme des données
        foreach($families as $family) {
            $id = $family->getId();
            $label = $family->getLabel();
            $depth = $family->getDepth();
            $id_parent = $family->getIdParents();
            $tabFam = array('id' => $id, 'label' => $label, 'depth' => $depth, 'id_parent' => $id_parent);
            $tabFamilies[] = $tabFam;
        }
        return $app->json($tabFamilies);

    }
}