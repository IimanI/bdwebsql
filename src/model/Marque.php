<?php
/**
 * Created by PhpStorm.
 * User: iman
 * Date: 25/04/15
 * Time: 21:59
 */

namespace bdwebsql\model;

/**
 * Class Marque
 * @package model
 * @Entity
 * @Table(name="marque")
 */
class Marque {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $libelle;


    public function getId(){

        return $this->id;
    }

    public function getLibelle(){

        return $this->libelle;
    }

    public function setLibelle($libelle){

        $this->libelle = $libelle;
    }

}