<?php
/**
 * Created by PhpStorm.
 * User: iman
 * Date: 25/04/15
 * Time: 21:37
 */

namespace bdwebsql\model;

/**
 * Class Family
 * @package model
 * @Entity
 * @Table(name="family")
 */
class Family {

    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="integer")
     */
    protected $id_parent;

    /**
     * @Column(type="string")
     */
    protected $label;

    /**
     * @Column(type="integer")
     */
    protected $depth;

    /**
     * @Column(type="integer")
     */
    protected $position;

    /**
     * @Column(type="string")
     */
    protected $date_create;

    /**
     * @Column(type="string")
     */
    protected $parents;

    /**
     * @ManyToMany(targetEntity="Article")
     * @JoinTable(name="family_article",
     *      joinColumns={@JoinColumn(name="id_family", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="id_article", referencedColumnName="id")})
     */
    private $articles;


    public function __construct(){

        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getArticles(){

        return $this->articles;
    }

    public function getId() {

        return $this->id;
    }

    public function getIdParents() {

        return $this->id_parent;
    }

    public function setIdParents($id_parent) {

        $this->id_parent = $id_parent;
    }

    public function getLabel() {

        return $this->label;
    }

    public function setLabel($label) {

        $this->label = $label;
    }

    public function getDepth() {

        return $this->depth;
    }

    public function setDepth($depth) {

        $this->depth = $depth;
    }

    public function getPosition() {

        return $this->position;
    }

    public function setIdPosition($position) {

        $this->position = $position;
    }

    public function getDateCreate() {

        return $this->date_create;
    }

    public function sedate_createte($date_create) {

        $this->date_create = $date_create;
    }

    public function getParents() {

        return $this->parents;
    }

    public function setParents($parents) {

        $this->parents = $parents;
    }

}