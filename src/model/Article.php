<?php
/**
 * Created by PhpStorm.
 * User: iman
 * Date: 24/04/15
 * Time: 23:39
 */

namespace bdwebsql\model;

/**
 * Class Article
 * @package model
 * @Entity  @Table(name="article")
 */
class Article {
    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $reference;

    /**
     * @Column(type="string")
     */
    protected $label;

    /**
     * @Column(type="integer")
     */
    protected $qte;

    /**
     * @Column(type="float")
     */
    protected $pu_ht;

    /**
     * @ManyToOne(targetEntity="Marque")
     *@JoinColumn(name="id_marque", referencedColumnName="id")
     */
    protected $marque;

    /**
     * @ManyToOne(targetEntity="Tva")
     *@JoinColumn(name="id_tva", referencedColumnName="id")
     */
    protected $tva;


    public function getId(){

        return $this->id;
    }

    public function getReference(){

        return $this->reference;
    }

    public function setReference($reference){

        $this->reference = $reference;
    }

    public function getLabel(){

        return $this->label;
    }

    public function setLabel($label){

        $this->label = $label;
    }

    public function getTva(){

        return $this->tva;
    }

    public function setTva($tva){

        $this->tva = $tva;
    }

    public function getQte(){

        return $this->qte;
    }

    public function setQte($qte){

        $this->qte = $qte;
    }

    public function getPuHt(){

        return $this->pu_ht;
    }

    public function setPuHt($pu_ht){

        $this->pu_ht = $pu_ht;
    }

    public function getMarque() {
        return $this->marque;
    }
}